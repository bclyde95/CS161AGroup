# CS161AGroup
#### A place for code and files relating to our **Winter Term 2022 CS161A** study group

## Folder Structure

* *studentName*
	* *studentFiles*



## Important Links & Resources

* [Replit](https://repl.it)

* [Algorithmic Design Document](https://docs.google.com/document/d/1Y0kHPoUWHcbH-_0yma7sMCUR0EvLL7CKEvTGTetqEmU/edit)

* [C++ Style Guide](https://docs.google.com/document/d/1avQh7119eRLYZg2ctgeJ57eNRr-KgLr56h2eBxi9_dQ/edit)
