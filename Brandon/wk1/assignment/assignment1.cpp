#include <iostream>

int main (void) {
    const float STEPS_IN_A_MILE = 2112.0;
    int numOfSteps;

    std::cout << "Enter number of steps taken: ";
    std::cin >> numOfSteps;
    std::cout << std::endl << std::endl;
    
    std::cout << numOfSteps / STEPS_IN_A_MILE << " miles" << std::endl;

    return 0;
}
