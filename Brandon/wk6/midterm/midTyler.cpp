/******************************************************************************
# Author:           Tyler Renn
# Assignment:   MidTerm
# Date:              Feb 16, 2022
# Description:   This program calculates a user’s airline fare based on age, carry-on bags and checked bags. The fares will be calculated in dollar amounts and the users total will be                                 displayed at the end of the program

# Input:            String Name
           Int userAge
           int carryOn
           Int checkBag

# Output:         Double airFare
#******************************************************************************/


#include <iostream>
#include <string>


using namespace std;
int main() {
    // User Inputs
    string fullName;
    int userAge;
    int carryOn;
    int checkBag;
    double airFare = 0;
    
    // Welcome Message
    cout << "Welcome to the Complex Airfare Calculator!\n" << endl;
    
    // Enter Name
    cout << "Please enter your name: ";
    getline (cin,fullName);
    cout << "Hello " << fullName << "!" << endl;
    
    // Enter Age
    cout << "Please enter age of the passenger: ";
    cin >> userAge;
    
    // Enter # of carry on's
    cout << "How many carry-on bags do you have (0 or 1)? ";
    cin >> carryOn;
    
        
    // Enter # of checked bags
    cout << "How many checked bags do you have? ";
    cin >> checkBag;
    
        // If-Else determining base price
        if ( (userAge > 2) && (userAge < 60) ) {
            airFare += 300.00;
        }
        else if (userAge >= 60) {
            airFare += 290.00;
        }
        
        else if ( ( userAge > 0) && (userAge <= 2) ) {
            airFare += 0.00;
        }
        
        else {
            cout << "INVALID AGE. Please rerun the program." << endl;
            return 0;
        }
            // If-Else determining carry on
            if (carryOn == 1) {
                airFare += 10.00;
            }
            else if (carryOn == 0) {
                airFare += 0.00;
            }
            else {
                cout << "\nYou have too many carry-on bags! Sorry you cannot fly ComplexAir!" << endl;
            }
            
                // Switch Statement determining checked bags
                switch (checkBag) {
                  case 0:
                  case 1:
                        cout << "Your airfare is: $" << airFare << endl;
                        cout << "Thank you for flying Complex Air!" << endl;
                     break;

                  case 2:
                        airFare += 25.00;
                        cout << "Your airfare is: $" << airFare << endl;
                        cout << "Thank you for flying Complex Air!" << endl;
                     break;

                  case 3:
                        airFare = airFare + 25.00 + 50.00;
                        cout << "Your airfare is: $" << airFare << endl;
                        cout << "Thank you for flying Complex Air!" << endl;
                     break;
                  
                  case 4:
                        airFare = airFare + 25.00 + (2 * 50.00);
                        cout << "Your airfare is: $" << airFare << endl;
                        cout << "Thank you for flying Complex Air!" << endl;
                    break;
                  
                  case 5:
                          airFare = airFare + 25.00 + (3 * 50.00);
                        cout << "Your airfare is: $" << airFare << endl;
                        cout << "Thank you for flying Complex Air!" << endl;
                    break;
                  
                  default:
                        cout << "A passenger may have no more than 5 checked bags!" << endl;
                     break;
                }
    
    
                
    return 0;
}
