DECLARE string name
DECLARE int age
DECLARE int carryOn
DECLARE int checkedBags
DECLARE int baseCost

DISPLAY "Welcome to the Complex Airfare Calculator!"

DISPLAY "Please enter your name: "
INPUT name
DISPLAY "Hello " name "!"

DISPLAY "Please enter age of the passenger: "
INPUT age
IF (age < 0 || age > 100) THEN
  DISPLAY "Invalid age! Sorry you cannot fly ComplexAir!"
  END PROGRAM
ELSE IF (age < 60 && age > 2) THEN
  SET baseCost = 300
ELSE IF (age >= 60) THEN
  SET baseCost = 290
END IF

DISPLAY "How many carry-on bags do you have (0 or 1)? "
INPUT carryOn
IF (carryOn != 0 && carryOn != 1) THEN
  DISPLAY "You have too many carry-on bags! Sorry you cannot fly ComplexAir!"
  END PROGRAM
ELSE IF (carryOn) THEN
  SET baseCost = baseCost + 10
END IF

DISPLAY "How many checked bags do you have? "
INPUT checkedBags
IF (checkedBags < 0 && checkedBags > 5) THEN
  DISPLAY "You have too many checked bags! Sorry you cannot fly ComplexAir!"
  END PROGRAM
ELSE IF (checkedBags > 1) THEN
  SET baseCost = baseCost + 25 + ((checkedBags - 2) * 50)
END IF

DISPLAY "Your airfare is: $" baseCost

DISPLAY "Thank you for flying ComplexAir!"