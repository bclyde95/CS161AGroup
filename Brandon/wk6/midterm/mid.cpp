/******************************************************************************
# Author:           Brandon Clyde
# Assignment:       Midterm 1
# Date:             02/16/2022
# Description:      This program computes the airfare for a ComplexAir flight. 
#                   It takes in name, age, carry-on bags, and checked bags and outputs the airfare.
# Input:            ("name", string); ("age", int); ("carryOn", int); ("checkedBags", int)
# Output:           ("baseCost", int)
# Sources:          None
#*****************************************************************************/

#include <iostream>

int main (void){
  // Declare variables
  std::string name;
  int age = 0;
  int carryOn = 0;
  int checkedBags = 0;
  int baseCost = 0;
  

  // Display welcome message
  std::cout << "Welcome to the Complex Airfare Calculator!"
            << std::endl
            << std::endl;

  // Get and output name
  std::cout << "Please enter your name: ";
  std::getline(std::cin, name);
  std::cout << "Hello "
            << name
            << "!"
            << std::endl;

  /*** Age and Base Cost ***/
  // Prompt and get age
  std::cout << "Please enter age of the passenger: ";
  std::cin >> age;

  // Determine base cost
  if (age < 0 || age > 100){
    std::cout << std::endl
              << "Invalid age! "
              << "Sorry you cannot fly ComplexAir!"
              << std::endl
              << std::endl;
    return 1;
  }
  else if (age < 60 && age > 2){
    baseCost = 300;
  }
  else if (age >= 60){
    baseCost = 290;
  }
  
  /*** Number of Carry on bags and carry on cost ***/
  // Prompt and get carryOn
  std::cout << "How many carry-on bags do you have (0 or 1)? ";
  std::cin >> carryOn;

  // Determine carry-on cost
  if (carryOn != 0 && carryOn != 1){
    std::cout << std::endl
              << "You have too many carry-on bags! " 
              << "Sorry you cannot fly ComplexAir!"
              << std::endl
              << std::endl;
    return 1;
  }
  else if (carryOn){
    baseCost += 10;
  }

  /*** Checked bags and cost ***/
  // Prompt and get checked bags

  std::cout << "How many checked bags do you have? ";
  std::cin >> checkedBags;

  if (checkedBags < 0 || checkedBags > 5){
    std::cout << std::endl
              << "You have too many checked bags! " 
              << "Sorry you cannot fly ComplexAir!"
              << std::endl
              << std::endl;
    return 1;
  }
  else if (checkedBags > 1){
    baseCost += 25 + ((checkedBags - 2) * 50);
  }

  // Output message
  std::cout << std::endl
            << "Your airfare is: $"
            << baseCost
            << std::endl;

  // Goodbye message
  std::cout << "Thank you for flying ComplexAir!"
            << std::endl
            << std::endl;

  return 0;
}
