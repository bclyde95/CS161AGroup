#/******************************************************************************
# Author:           Onna Giddens 
# Assignment:       Mid Term Exam
# Date:             2/16/2022
# Description:      This program will calculate the total cost of an airline 
#                   ticket including bags and carry-on for each user. Users will
#                   enter their name age and number of carry-on bags and checked
#                   bags for the flight and the program will tell the user the 
#                   total cost of the ticket with bag fees included. 
# Input:            Name - string
#                   Age - integer 
#                   carry-on (0 or 1) integer
#                   checked bags  integer#                  
# Output:           Total airfare - float
#                   
# Sources:          Midterm Exam Version B
#                   
#******************************************************************************/

#include <iostream>
#include <string>
#include <cmath>
#include <iomanip>
using namespace std;
int main() {

    string nameUser = " ";
    int ageUser = 0;
    int carryOnTotal = 1; 
    float carryOnPrice = 0.0;
    int userCarryOn = 0;
    const int checkBagTotal = 5;
    int userCheckBag = 0;
    float checkBagPrice = 0.0;
    const int oneBag = 25;
    const int twoPlusBag = 50;
    const int normalTix = 300;
    const int childTix = 0;
    const int seniorTix = 290;
    float totalTixPrice = 0.0;
    float passengerTixPrice = 0.0;

cout << endl;
cout << "Welcome to the Complex Airfare Calculator!" << endl;
cout << endl;
cout << "Please enter your name: " ;
cin >> nameUser; 
cout << "Hello " << nameUser << " !" << endl;

cout << "Please enter age of the passenger:";
cin >> ageUser;


if ((ageUser > 2)  && (ageUser < 60)) {
    passengerTixPrice = normalTix;
}
else if (ageUser >= 60) {
    passengerTixPrice = seniorTix;
}
else if (ageUser <= 2) {

    passengerTixPrice = childTix;
}

cout << "How many carry bags do you have? ";
cin >> userCarryOn;

if (userCarryOn > carryOnTotal ) {
    cout << "You have too many carry-on bags! Sorry, you cannot fly ComplexAir!" << endl; 
}
else{
    carryOnPrice = userCarryOn * 10;
    }

cout << "How many checked bags do you have? ";
cin >> userCheckBag;

if(userCheckBag > checkBagTotal) {
    cout << " You have too many carry-on bags! Sorry, you cannot fly ComplexAir!" << endl;
}
else if(userCheckBag == oneBag ) {
    checkBagPrice = oneBag;
}
else if (userCheckBag > 1) {
    checkBagPrice = ((userCheckBag - 1) * 50) - 25;
}

cout << endl; 
totalTixPrice = passengerTixPrice + carryOnPrice + checkBagPrice;

cout <<fixed << setprecision(2) << "Your airfare is: $" <<totalTixPrice << endl;
cout << " Thank you for flying ComplexAir!"<< endl;




        return 0;
}
