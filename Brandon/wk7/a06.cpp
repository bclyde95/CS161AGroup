/******************************************************************************
# Author:           Brandon Clyde
# Assignment:       Assignment 6
# Date:             02/26/2022
# Description:      This program continuously estimates the number of gumballs within a jar,
#                   based on the volume of the jar and the volume of each gumball;
#                   until the user stops the input.  
#                   It repeatedly takes in the jar volume in mL and calculates the volume of
#                   a gumball from the user input radius in cm; then outputs the estimated number of gumballs.
#                   After the user stops the input, the total number of entries, the total gumballs,
#                   the largest gumball, and the jar with the most gumballs are calculated and displayed to the user.
# Input:            (“gumballRadius”, double); (“jarVolume”, double); (“continueLoop”, char);
# Output:           (“numOfGumballs”, int); (“totalJars”, int); (“averageGumballs”, double); 
#                   (“largestGumball”, double); (“maxGumballJar”, int)
# Sources:          None
#*****************************************************************************/

#define _USE_MATH_DEFINES
#include <iostream>
#include <iomanip>
#include <cmath>
#include <functional>

/*** CONSTANT DECLARATIONS ***/
const double PI = M_PI;
const double LOAD_FACTOR = 0.64;


/*** FUNCTION DECLARATIONS ***/
double volumeOfSphere (double radius);
int estimateGumballs (double gumballRadius, double jarVolume);
template <typename T>
T validInput (std::string msg, std::string type, std::function<bool(T)> func = [](T x) -> bool {return false;});


/*** MAIN ***/
int main (void) {
  // Declare and initialize variables
  double gumballRadius = 0.0;
  double jarVolume = 0.0;
  double gumballVolume = 0.0;
  int numOfGumballs = 0;
  char continueLoop = 'y';

  int totalGumballs = 0;
  int totalJars = 0;
  double largestGumball = 0;
  int maxGumballs = 0;
  int maxGumballJar = 0;

  // Set precision
  std::cout << std::fixed 
            << std::setprecision(2);

  // Print program welcome statement
  std::cout << "Welcome to my Gumball Guesser program!"
            << std::endl
            << std::endl;

  while(continueLoop == 'y'){

    // Prompt user, then store gumballRadius and jarVolume into their respective variables
    std::cout << "Enter the radius of a gumball (cm)"
              << std::endl
              << "and the volume of a jar (mL) separated by a space: ";
    std::cin >> gumballRadius;
    std::cin >> jarVolume;

    // Find estimate and save to numOfGumballs for output
    numOfGumballs = estimateGumballs(gumballRadius, jarVolume);

    // Output message
    std::cout << "Estimate of gumballs in the jar: " 
              << numOfGumballs
              << std::endl
              << std::endl;

    // Increment totalJars 
    totalJars++;

    // Add numOfGumballs to totalGumballs
    totalGumballs += numOfGumballs;

    // Update maxGumball if a larger gumball is found
    gumballVolume = volumeOfSphere(gumballRadius);
    largestGumball = gumballVolume > largestGumball ? gumballVolume : largestGumball;

    // Update maxGumballJar if numOfGumballs is greater that maxGumballs
    if (numOfGumballs > maxGumballs){
      maxGumballs = numOfGumballs;
      maxGumballJar = jarVolume;
    }

    // Prompt user to continue, break loop if 'n'
    continueLoop = validInput<char>("Do you want to enter more (y/n): ", "char");
    std::cout << std::endl;
  }

  // Statistics message
  std::cout << "Number of entries: "
            << totalJars
            << std::endl
            << "Average number of gumballs: "
            << totalGumballs / static_cast<double>(totalJars)
            << std::endl
            << "Largest gumball: "
            << largestGumball
            << " cm^3"
            << std::endl
            << "Jar size for largest gumball estimate: "
            << maxGumballJar
            << " mL"
            << std::endl
            << std::endl;

  // Thank you message
  std::cout << "Thank you for using my program!" 
            << std::endl;
  
  return 0;
}


/*** FUNCTION DEFINITIONS ***/
/*
volumeOfSphere:
  Finds the volume of a sphere given the radius,
  then returns the volume as a double
*/
double volumeOfSphere (double radius) {
  return (4.0/3) * PI * pow(radius, 3);
}

/*
estimateGumballs:
  Estimates the amount gumballs within a jar
  given the radius of a gumball and the volume
  of the jar. The estimation assumes that the
  gumballs take up 64% of the total jar volume.
  Returns the gumball estimate as an int.
*/
int estimateGumballs (double gumballRadius, double jarVolume) {
  return floor((jarVolume / volumeOfSphere(gumballRadius)) * LOAD_FACTOR);
}

/* 
validInput<type>:
  Prompts user with an input message then 
  verifies that the input is of type <type>.
  Loops until a valid input is entered.
  Takes in an optional function to check 
  the input against an additional condition.
  Returns a validated <type>.
  Function is a generic, return type is a parameter.
*/
template <typename T>
T validInput (std::string msg, std::string type, std::function<bool(T)> func){
  T tmp;

  // Prompt user with "msg" and get user input
  std::cout << msg;
  std::cin >> tmp;

  // Check for an error on the input via cin, then loop until the input is valid
  while(true){
    if (std::cin.fail()){
      // Clear and ignore buffer so it is not an endless loop
      std::cin.clear();
      std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
      // Display error message and re prompt
      std::cout << "Invalid input! Input should be of type '" 
                << type 
                << "'" 
                << std::endl
                << std::endl
                << msg;
      std::cin >> tmp;
    }
    else if (func(tmp)){
      // Check custom condition and reprompt if true
      // Clear and ignore buffer so it is not an endless loop
      std::cin.clear();
      std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
      // Display error message and re prompt
      std::cout << "Invalid value!"
                << std::endl
                << std::endl
                << msg;
      std::cin >> tmp;
    }
    else{
      break;
    }    
  }

  // return validated input
  return tmp;
}
