/******************************************************************************
# Author:           Brandon Clyde
# Assignment:       Final Exam
# Date:             03/23/2022
# Description:      This program takes in a user password and validates it based on conditions.
#                   It then outputs the errors if there are any. If not, it prints “OK”
# Input:            ("userPass", string);
# Output:           ("err", string);
# Sources:          None
#*****************************************************************************/

#include <iostream>
#include <cctype>

/*** FUNCTION DECLARATIONS ***/
void welcomeMessage();
bool passErr(bool tooShort, bool hasLetter, bool hasNum, bool hasSpec);
void validPass();
void endMessage();

int main (void){
  welcomeMessage();

  validPass();

  endMessage();

  return 0;
}

void welcomeMessage(){
  std::cout << "Welcome to my Final Exam"
            << " - Password Requirements."
            << std::endl
            << std::endl;
}

void validPass(){
  std::string userPass;
  bool tooShort = false; 
  bool hasLetter = false; 
  bool hasNum = false; 
  bool hasSpec = false;

  // Prompt user for input
  std::cout << "Please enter the password: ";

  std::getline(std::cin, userPass);

  std::cout << std::endl
            << "The output is:"
            << std::endl;
  
  // Set tooShort if userPass has less than 8 characters 
  if (userPass.length() < 8){
    tooShort = true;
  }
  
  for (int i = 0; i < userPass.length(); i++){
    if (isalpha(userPass.at(i)) && !hasLetter){
      hasLetter = true;
    }
    if (isdigit(userPass.at(i)) && !hasNum){
      hasNum = true;
    }
    if ((userPass.at(i) == '!'  ||
         userPass.at(i) == '#'  ||
         userPass.at(i) == '%') &&
         !hasSpec
    ){
      hasSpec = true;
    }
  }

  // If no errors, otherwise passErr will print errors
  if(!passErr(tooShort, hasLetter, hasNum, hasSpec)){
    std::cout << "OK"
              << std::endl;
  }

  std::cout << std::endl;

}

bool passErr(bool tooShort, bool hasLetter, bool hasNum, bool hasSpec){
  bool hasErr = false;
  if(tooShort){
    std::cout << "Too short"
              << std::endl;
    hasErr = true;
  }
  if(!hasLetter){
    std::cout << "Missing letter"
              << std::endl;
    hasErr = true;
  }
  if(!hasNum){
    std::cout << "Missing number"
              << std::endl;
    hasErr = true;
  }
  if(!hasSpec){
    std::cout << "Missing special character"
              << std::endl;
    hasErr = true;
  }

  return hasErr;
}

void endMessage(){
  std::cout << "Thank you for using my "
            << "Password Requirements program!"
            << std::endl;
}
