/******************************************************************************
# Author:           Brandon Clyde
# Assignment:       Assignment 4
# Date:             02/05/2022
# Description:      TriMet Hop Fastpass Calculator. This program calculates 
#                   the amount paid for tickets in the month and amount 
#                   until free rides are earned. Given the number of 
#                   tickets purchased and the type of pass.
# Input:            (“passType”, char); (“numTickets”, int);
# Output:           (“numTickets”, int); (“amountPaid”, double); (“amountToFree”, double);
# Sources:          https://www.geeksforgeeks.org/passing-a-function-as-a-parameter-in-cpp/
#                   https://docs.microsoft.com/en-us/cpp/cpp/lambda-expressions-in-cpp
#*****************************************************************************/

#include <iostream>
#include <iomanip>
#include <limits>
#include <functional>

/*** CONSTANT DECLARATIONS ***/
const int ADULT_FREE = 100;
const int DISCOUNT_FREE = 28;
const double ADULT_PRICE = 2.50;
const double DISCOUNT_PRICE = 1.25;


/*** FUNCTION DECLARATIONS ***/
// Input Validation
char fastpassInput(std::string msg);
template <typename T>
T validInput (std::string msg, std::string type, std::function<bool(T)> func = [](T x) -> bool {return false;});

// Output calculations and helpers
void fastPassInfo(void);
void rideInfo(void);
double amountPaid(int numTickets, char passType);
double amountToFree(int numTickets, char passType);
void outputMsg(int numTickets, char passType);



/*** MAIN ***/
int main(void){
  // Declare and initialize variables
  char passType = ' ';
  int numTickets = 0;

  // Set precision
  std::cout << std::fixed 
            << std::setprecision(2);

  // Welcome message
  std::cout << "Welcome to TriMet Hop Fastpass!"
            << std::endl
            << std::endl;

  // Display fastpass and ride info
  fastPassInfo();
  rideInfo();

  // Inputs
  passType = fastpassInput("Enter Fastpass (A,H,Y): ");
  std::cout << std::endl;
  numTickets = validInput<int>("Enter the number tickets purchased this month: ", "int",
                // Make sure input is greater than 0
                [](int x){
                  return x < 0;
                }
               );

  // Output messages
  outputMsg(numTickets, passType);

  // Goodbye message
  std::cout << "Thank you for riding TriMet!"
            << std::endl;

  return 0;
}



/*** FUNCTION DEFINITIONS ***/
// Input Validation
/* 
fastPassInput:
  Prompts user with an input message then 
  verifies that the input is of type char,
  also checking if the char is a valid option.
  Loops until a valid input is entered.
  Returns a validated char 'A', 'H', or 'Y'.
  Input is case insensitive.
*/
char fastpassInput(std::string msg) {
  char tmp;

  // Prompt user with "msg" and get user input
  std::cout << msg;
  std::cin >> tmp;

  while(true){
    if (std::cin.fail()){
      // Clear and ignore buffer so it is not an endless loop
      std::cin.clear();
      std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
      // Display error message and re prompt
      std::cout << "Invalid input! Input should be of type 'char'" 
                << std::endl
                << std::endl
                << msg;
      std::cin >> tmp;
    }
    else if (tmp == 'a' || 
             tmp == 'A' || 
             tmp == 'h' || 
             tmp == 'H' ||
             tmp == 'y' ||
             tmp == 'Y' ){
      break;
    }
    else{
      // Clear and ignore buffer so it is not an endless loop
      std::cin.clear();
      std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
      // Display error message and re prompt
      std::cout << "Invalid pass! Please enter (A, H, or Y)" 
                << std::endl
                << std::endl
                << msg;
      std::cin >> tmp;
    }  
  }

  return tmp;
}


/* 
validInput<type>:
  Prompts user with an input message then 
  verifies that the input is of type <type>.
  Loops until a valid input is entered.
  Takes in an optional function to check 
  the input against an additional condition.
  Returns a validated <type>.
  Function is a generic, return type is a parameter.
*/
template <typename T>
T validInput (std::string msg, std::string type, std::function<bool(T)> func){
  T tmp;

  // Prompt user with "msg" and get user input
  std::cout << msg;
  std::cin >> tmp;

  // Check for an error on the input via cin, then loop until the input is valid
  while(true){
    if (std::cin.fail()){
      // Clear and ignore buffer so it is not an endless loop
      std::cin.clear();
      std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
      // Display error message and re prompt
      std::cout << "Invalid input! Input should be of type '" 
                << type 
                << "'" 
                << std::endl
                << std::endl
                << msg;
      std::cin >> tmp;
    }
    else if (func(tmp)){
      // Check custom condition and reprompt if true
      // Clear and ignore buffer so it is not an endless loop
      std::cin.clear();
      std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
      // Display error message and re prompt
      std::cout << "Invalid value!"
                << std::endl
                << std::endl
                << msg;
      std::cin >> tmp;
    }
    else{
      break;
    }    
  }

  // return validated input
  return tmp;
}


// Output calculations and helpers
/* 
fastPassInfo:
  Displays information about the different Fastpass passes
  in a table like format that emphasizes readability.
  Prints to the console, does not return any values.
*/
void fastPassInfo(void){
  // Header
  std::cout << "Fastpass Choices"
            << std::setfill(' ')
            << std::setw(32)
            << "Ticket $"
            << std::endl;

  // Horizontal line
  std::cout << std::setfill('-') 
            << std::setw(40) 
            << "  "
            << std::setfill('-')
            << std::setw(8)
            << ""
            << std::endl;

  // Adult
  std::cout << "A. Adult (ages 18-64)"
            << std::setfill(' ')
            << std::setw(25)
            << "$"
            << ADULT_PRICE
            << std::endl;

  // Honored Citizen
  std::cout << "H. Honored Citizen (65+, disabilities)"
            << std::setfill(' ')
            << std::setw(8)
            << "$"
            << DISCOUNT_PRICE
            << std::endl;

  // Youth
  std::cout << "Y. Youth (ages 7-17)"
            << std::setfill(' ')
            << std::setw(26)
            << "$"
            << DISCOUNT_PRICE
            << std::endl;

  std::cout << std::endl;
}


/* 
rideInfo:
  Displays information about riding with TriMet.
  Contains info like how long each ticket is good for
  and the amount one has to spend to earn free rides.
  Prints to the console, does not return any values.
*/
void rideInfo(void){
  std::cout << "Note: Ride for 2 hours and 30 minutes with each ticket."
            << std::endl
            << std::endl
            << "Ride free for the rest of the month after spending $"
            << ADULT_FREE
            << std::endl
            << "with an Adult pass, or $"
            << DISCOUNT_FREE
            << " with an Honored Citizen or"
            << std::endl
            << "Youth pass!"
            << std::endl
            << std::endl;
}


/* 
amountPaid:
  Calculates the amount of money paid in the month
  from the number of tickets and the type of pass.
  Returns a double.
*/
double amountPaid(int numTickets, char passType){
  if (passType == 'a' || passType == 'A'){
    return numTickets * ADULT_PRICE;
  }
  return numTickets * DISCOUNT_PRICE;
}


/* 
amountToFree:
  Calculates the amount of money that must be spent
  to earn free rides for the rest of the month, with
  respect to the number of tickets and the type of pass.
  Returns a double.
*/
double amountToFree(int numTickets, char passType){
  if (passType == 'a' || passType == 'A'){
    return ADULT_FREE - amountPaid(numTickets, passType);
  }
  return DISCOUNT_FREE - amountPaid(numTickets, passType);
}


/* 
outputMsg:
  Displays formatted outputs from calculation functions.
  Proper grammar and plurals are preserved.
  Prints to the console, does not return any values.
*/
void outputMsg(int numTickets, char passType){
  double tmpFree;

  // Tickets
  std::cout << std::endl
            << "You have purchased "
            << numTickets;
  if (numTickets < 2){
    std::cout << " ticket!"
              << std::endl;
  }
  else{
    std::cout << " tickets!"
              << std::endl;
  }

  // Paid
  std::cout << "You have paid: $ " 
            << amountPaid(numTickets, passType)
            << std::endl;

  // Free rides
  tmpFree = amountToFree(numTickets, passType);
  if (tmpFree <= 0){
    std::cout << "You have earned free rides for the rest of the month!";
  }
  else{
    std::cout << "Spend $ "
              << tmpFree
              << " more to earn free rides for the rest of the month!";
  }

  std::cout << std::endl
            << std::endl;
}
