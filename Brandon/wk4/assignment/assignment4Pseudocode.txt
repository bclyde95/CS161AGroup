DECLARE const int ADULT_FREE
DECLARE const int DISCOUNT_FREE
DECLARE const double ADULT_PRICE
DECLARE const double DISCOUNT_PRICE

SET ADULT_FREE = 100
SET DISCOUNT_FREE = 28
SET ADULT_PRICE = 2.50
SET DISCOUNT_PRICE = 1.25

DECLARE char passType
DECLARE int numTickets

FUNCTION double 
amountPaid(int numTickets, char passType)
  IF (passType == 'a' || passType == 'A') THEN
    RETURN numTickets * ADULT_PRICE
  ELSE
    RETURN numTickets * DISCOUNT_PRICE
  END IF
END FUNCTION

FUNCTION double 
amountToFree(int numTickets, char passType)
  IF (passType == 'a' || passType == 'A') THEN
    RETURN ADULT_FREE - amountPaid(numTickets, passType)
  ELSE
    RETURN DISCOUNT_FREE - amountPaid(numTickets, passType)
  END IF
END FUNCTION

DISPLAY “Welcome to TriMet Hop Fastpass!”

DISPLAY “Fastpass Choices                        Ticket $”
DISPLAY “--------------------------------------  -–------”
DISPLAY “A. Adult (ages 18-64)                    $2.50”
DISPLAY “H. Honored Citizen (65+, disabilities)   $1.25"
DISPLAY “Y. Youth (ages 7-17)                     $1.25”

DISPLAY “Note: Ride for 2 hours and 30 minutes with each ticket.”

DISPLAY “Ride free for the rest of the month after spending $100 with an Adult pass, or $28 with an Honored Citizen or Youth pass!”

DISPLAY “Enter Fastpass (A, H, Y): ”
INPUT passType

DISPLAY “Enter the number tickets purchased this month: ”
INPUT numTickets


DISPLAY “You have purchased “ numTickets ” tickets!”
DISPLAY “You have paid: $ ” amountPaid(numTickets, passType)
IF (amountToFree(numTickets, passType) <= 0) THEN
    DISPLAY “You have earned free rides for the rest of the month!”
ELSE
    DISPLAY “Spend $ “ amountToFree(numTickets, passType) “ more to earn free rides for the rest of the month!”
END IF

DISPLAY “Thank you for riding TriMet!”
