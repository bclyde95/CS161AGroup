DECLARE const double LOAD_FACTOR
DECLARE double gumballRadius
DECLARE double jarVolume
DECLARE int numOfGumballs

SET LOAD_FACTOR = 0.64

FUNCTION double
volumeOfSphere (double radius)
    RETURN   (4.0/3) * PI * pow(radius, 3)
END FUNCTION

FUNCTION int
estimateGumballs (double gumballRadius, double jarVolume)
    RETURN floor((jarVolume / volumeOfSphere(gumballRadius)) * LOAD_FACTOR)
END FUNCTION

DISPLAY “Welcome to my gumball guesser program!”

DISPLAY “Enter the radius of a gumball (cm) and the volume of a jar (mL) separated by a space:
INPUT gumballRadius
INPUT jarVolume

SET numOfGumballs = estimateGumballs(gumballRadius, jarVolume)

DISPLAY “Estimate of gumballs in the jar: “ numOfGumballs

DISPLAY “Thank you for using my program!”

