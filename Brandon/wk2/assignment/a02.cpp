#define _USE_MATH_DEFINES
#include <iostream>
#include <cmath>

/*** CONSTANT DECLARATIONS ***/
const double PI = M_PI;
const double LOAD_FACTOR = 0.64;


/*** FUNCTION DECLARATIONS ***/
double volumeOfSphere (double radius);
int estimateGumballs (double gumballRadius, double jarVolume);


/*** MAIN ***/
int main (void) {
  // Declare and initialize variables
  double gumballRadius = 0.0;
  double jarVolume = 0.0;
  int numOfGumballs = 0;

  // Print program welcome statement
  std::cout << "Welcome to my gumball guesser program!"
            << std::endl
            << std::endl;

  // Prompt user, then store gumballRadius and jarVolume into their respective variables
  std::cout << "Enter the radius of a gumball (cm) and the volume of a jar (mL) separated by a space: ";
  std::cin >> gumballRadius;
  std::cin >> jarVolume;

  // Find estimate and save to numOfGumballs for output
  numOfGumballs = estimateGumballs(gumballRadius, jarVolume);

  // Output message
  std::cout << std::endl
            << "Estimate of gumballs in the jar: " 
            << numOfGumballs
            << std::endl
            << std::endl;

  // Thank you message
  std::cout << "Thank you for using my program!" 
            << std::endl;
  
  return 0;
}


/*** FUNCTION DEFINITIONS ***/
/*
volumeOfSphere:
  Finds the volume of a sphere given the radius,
  then returns the volume as a double
*/
double volumeOfSphere (double radius) {
  return (4.0/3) * PI * pow(radius, 3);
}

/*
estimateGumballs:
  Estimates the amount gumballs within a jar
  given the radius of a gumball and the volume
  of the jar. The estimation assumes that the
  gumballs take up 64% of the total jar volume.
  Returns the gumball estimate as an int.
*/
int estimateGumballs (double gumballRadius, double jarVolume) {
  return floor((jarVolume / volumeOfSphere(gumballRadius)) * LOAD_FACTOR);
}