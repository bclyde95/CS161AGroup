/******************************************************************************
# Author:           Brandon Clyde
# Assignment:       Assignment 8
# Date:             03/13/2022
# Description:      This is a coffee shop menu program. It displays options for 
#                   users to make selections from and adds them to a running total.
#                   When the selection process is complete, it prompts for a tip and 
#                   outputs the total amount to be paid.
# Input:            (“pathNum”, int); (“selectChar”, char); (“tipAmount”, double);
# Output:           (“total”, double);
# Sources:          None
#*****************************************************************************/

#include <iostream>
#include <iomanip>
#include <cmath>
#include <functional>

/*** CONSTANT DECLARATIONS ***/
const double DONUT_PRICE = 4.00;
const double MUFFIN_PRICE = 4.50;
const double PASTRY_PRICE = 5.50;
const double BAGEL_PRICE = 3.75;
const double TOAST_PRICE = 2.25;
const double COFFEE_PRICE = 3.50;
const double TEA_PRICE = 2.50;


/*** FUNCTION DECLARATIONS ***/
template <typename T>
T validInput (std::string msg, std::string type, std::function<bool(T)> func = [](T x) -> bool {return false;}, std::string errMsg="Invalid value!");
double getItemPrice (char selection, bool isDrink=false);
void printTotal(std::string msg, double total);
void welcomeMessage();
double menu();
double menuSweets();
double menuBread();
double menuDrinks();
double tipSuggestor(double total);


/*** MAIN ***/
int main(void){
  // Declare and initialize variables
  double total = 0.0;

  // Set precision
  std::cout << std::fixed 
            << std::setprecision(2);

  welcomeMessage();

  // Menu loop
  total = menu();

  // Display pre-tip total
  printTotal("Your total: ", total);
  std::cout << std::endl;

  // Prompt and add tip
  total += tipSuggestor(total);

  // Display amount to be paid
  printTotal("Please pay $", total);
  std::cout << std::endl;
            

  // Goodbye message
  std::cout << "Thank you for stopping by!"
            << std::endl;

  return 0;
}


/*** FUNCTION DEFINITIONS ***/

/*
welcomeMessage:
  Prints a welcome message
  Does not return a value
*/
void welcomeMessage(){
  std::cout << "Welcome to my Coffee Shop!"
            << std::endl;
}

/*
menu:
  Displays possible menu paths and prompts
  the user to choose one of the paths.
  Gets and validates path choice,
  then runs the correct menu path.
  Loops until the user chooses 4,
  displaying the total after each addition.
  Returns the total as a double
*/
double menu(){
  int pathNum = 0;
  double total = 0.0;

  do{
    // Print main menu
    std::cout << std::endl
              << "Please pick an option below:"
              << std::endl
              << "    1. Donuts/Muffins/Pastries"
              << std::endl
              << "    2. Bagels/Toast"
              << std::endl
              << "    3. Coffee/Tea"
              << std::endl
              << "    4. Quit"
              << std::endl;

    // Get and validate path choice
    pathNum = validInput<int>(
      ">> ",
      "int",
      [](int i){ return i < 1 || i > 4; },
      "Invalid Option! Please choose 1-4!\n>> "
    );

    // Direct to corresponding submenu
    if (pathNum == 1){
      total += menuSweets();
    }
    else if (pathNum == 2){
      total += menuBread();
    }
    else if (pathNum == 3){
      total += menuDrinks();
    }
    // print total after each selection
    printTotal("Total: ", total);
  }
  while (pathNum != 4);

  return total;
}

/*
menuSweets:
  Displays possible menu options and prices for:
  donuts, muffins, and pastries.
  Prompts user to input a letter
  Returns the price of the user selection.
*/
double menuSweets(){
  // Declare variable
  char selectChar;

  // Initial prompt
  std::cout << "Would you like"
            << std::endl
            << "    D: Donuts $"
            << DONUT_PRICE
            << std::endl
            << "    M: Muffins $"
            << MUFFIN_PRICE
            << std::endl
            << "    P: Pastries $"
            << PASTRY_PRICE
            << std::endl;

  // Prompt and store selectChar
  selectChar = validInput<char>(
    ">> ",
    "char",
    [](char c){ return tolower(c) != 'd' && tolower(c) != 'm' && tolower(c) != 'p'; },
    "Invalid Option!\nWould you like Donuts (D), Muffins (M), or Pastries (P): "
  );

  // Print item added and return price
  return getItemPrice(selectChar);
}

/*
menuBread:
  Displays possible menu options and prices for:
  bagels and toast.
  Prompts user to input a letter
  Returns the price of the user selection.
*/
double menuBread(){
  // Declare variable
  char selectChar;

  // Initial prompt
  std::cout << "Would you like"
            << std::endl
            << "    B: Bagels $"
            << BAGEL_PRICE
            << std::endl
            << "    T: Toast $"
            << TOAST_PRICE
            << std::endl;

  // Prompt and store selectChar
  selectChar = validInput<char>(
    ">> ",
    "char",
    [](char c){ return tolower(c) != 'b' && tolower(c) != 't'; },
    "Invalid Option!\nWould you like Bagels (B) or Toast (T): "
  );

  // Print item added and return price
  return getItemPrice(selectChar);
}

/*
menuDrinks:
  Displays possible menu options and prices for:
  coffee and tea.
  Prompts user to input a letter
  Returns the price of the user selection.
*/
double menuDrinks(){
  // Declare variable
  char selectChar;

  // Initial prompt
  std::cout << "Would you like"
            << std::endl
            << "    C: Coffee $"
            << COFFEE_PRICE
            << std::endl
            << "    T: Tea $"
            << TEA_PRICE
            << std::endl;

  // Prompt and store selectChar
  selectChar = validInput<char>(
    ">> ",
    "char",
    [](char c){ return tolower(c) != 'c' && tolower(c) != 't'; },
    "Invalid Option!\nWould you like Coffee (C) or Tea (T): "
  );

  // Print item added and return price
  return getItemPrice(selectChar, true);
}

/*
tipSuggestor:
  Displays suggested tip amounts.
  Prompts user to input a tip amount
  Returns the tip amount.
*/
double tipSuggestor(double total){
  // Declare variable
  double tipAmount = 0.0;

  // Tip suggestion
  std::cout << "Would you like to add a tip? "
            << "Suggested amounts:"
            << std::endl
            << std::endl
            << "10% = $"
            << total * 0.1
            << std::endl
            << "15% = $"
            << total * 0.15
            << std::endl
            << "20% = $"
            << total * 0.2
            << std::endl
            << std::endl;

  // Prompt and store tip amount
  tipAmount = validInput<double>(
    "Enter tip amount: $",
    "double",
    [](double tip){ return tip < 0; },
    "Invalid input!\nPlease enter a number 0 or greater: $"
  );

  return tipAmount;
}


/*
getItemPrice:
  Acts similar to a database for prices.
  Takes in a selection char and a default isDrink boolean.
  isDrink is false by default.
  Matches the selection to the item, outputs the item added, 
  then returns the item price.
  Returns a double.
*/
double getItemPrice (char selectChar, bool isDrink){
  selectChar = tolower(selectChar);

  // If not in drink section of menu, check only non-drink items 
  // If in drink section, check only drink items
  if (!isDrink){
    switch(selectChar){
      // Donuts
      case 'd':
        std::cout << "Donuts added."
                  << std::endl;
        return DONUT_PRICE;
      // Muffins
      case 'm':
        std::cout << "Muffins added."
                  << std::endl;
        return MUFFIN_PRICE;
      // Pastries
      case 'p':
        std::cout << "Pastries added."
                  << std::endl;
        return PASTRY_PRICE;
      // Bagels
      case 'b':
        std::cout << "Bagels added."
                  << std::endl;
        return BAGEL_PRICE;
      // Toast
      case 't':
        std::cout << "Toast added."
                  << std::endl;
        return TOAST_PRICE;
      
      default:
        return 0.0;
    }
  }
  else{
    // Coffee
    if(selectChar == 'c'){
      std::cout << "Coffee added."
                << std::endl;
      return COFFEE_PRICE;
    }
    // Tea
    else if(selectChar == 't'){
      std::cout << "Tea added."
                << std::endl;
      return TEA_PRICE;
    }
  }

  return 0.0;
}


/*
printTotal:
  Prints a message and the total.
  Does not return a value.
*/
void printTotal(std::string msg, double total){
  std::cout << msg
            << total
            << std::endl;
}


/* 
validInput<type>:
  Prompts user with an input message then 
  verifies that the input is of type <type>.
  Loops until a valid input is entered.
  Takes in an optional function to check 
  the input against an additional condition.
  Also takes an optional error message string
  Returns a validated <type>.
  Function is a generic, return type is a parameter.
*/
template <typename T>
T validInput (std::string msg, std::string type, std::function<bool(T)> func, std::string errMsg){
  T tmp;

  // Prompt user with "msg" and get user input
  std::cout << msg;
  std::cin >> tmp;

  // Check for an error on the input via cin, then loop until the input is valid
  while(true){
    if (std::cin.fail()){
      // Clear and ignore buffer so it is not an endless loop
      std::cin.clear();
      std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
      // Display error message and re prompt
      std::cout << "Invalid input! Input should be of type '" 
                << type 
                << "'" 
                << std::endl
                << std::endl
                << msg;
      std::cin >> tmp;
    }
    else if (func(tmp)){
      // Check custom condition and reprompt if true
      // Clear and ignore buffer so it is not an endless loop
      std::cin.clear();
      std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
      // Display error message and re prompt
      std::cout << errMsg;
      std::cin >> tmp;
    }
    else{
      // return validated input
      return tmp;
    }
  }
}
