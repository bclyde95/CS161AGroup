/******************************************************************************
# Author:           Erin Egloff and Brandon Clyde
# Lab:              Discussion #3
# Date:             January 26, 2022
# Description:      This program will show two numbers to be added together. Once the user presses "enter", the program will display the answer.
# Input:      *press* "enter"      
# Output:           sum of two random numbers
# Sources:          
#******************************************************************************/

#include <iostream>
#include <iomanip>
#include <limits>
#include <cstdlib>
#include <ctime>

/*** MAIN ***/
int main() {
  // Declare outputs
  int randNum1 = 0;
  int randNum2 = 0;
  int numSum = 0;

  
  // Set randomization
  srand(time(0));
  randNum1 = (rand() % 1000);
  randNum2 = (rand() % 1000);

  // Display description
  std::cout << "This program will add two random numbers and display the result!" << std::endl;
  
  std::cout << std::setw(5) 
            << std::right 
            << randNum1
            << std::endl;

  std::cout << "+"
            << std::setw(4) 
            << std::right 
            << randNum2
            << std::endl;

  std::cout << std::setfill('-') 
            << std::setw(6) 
            << ""
            << std::endl
            << std::endl;
  
  // Get input
  std::cout << "Press ENTER to display the result: "           << std::endl;
  std::cin.get();

  // Calculate sum of random numbers
  numSum = randNum1 + randNum2;

  // Output message
  std::cout << std::setfill(' ') 
            << std::setw(6) 
            << ""
            << std::endl
            << std::endl;
  
  std::cout << std::setw(5) 
            << std::right 
            << randNum1
            << std::endl;

  std::cout << "+"
            << std::setw(4) 
            << std::right 
            << randNum2
            << std::endl;

  std::cout << std::setfill('-') 
            << std::setw(6) 
            << ""
            << std::endl;
  
  std::cout << std::setfill(' ') 
            << std::setw(8) 
            << " "
            << std::endl;

  std::cout << std::setw(5)
            << std::right
            << numSum
            << std::endl
            << std::endl;

  std::cout << "Thank you for using this program!"
            << std::endl;

  return 0;
}
