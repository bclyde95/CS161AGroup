DECLARE const int OREOS_PER_SERVING
DECLARE const int CALORIES_PER_OREO

DECLARE int numEaten

DECLARE double servings
DECLARE int totalCalories

DISPLAY "Welcome to the Oreo Calculator!"

DISPLAY "Enter the number of Oreos eaten:"
INPUT numEaten

SET servings = numEaten / double(OREOS_PER_SERVING)
SET totalCalories = numEaten * CALORIES_PER_OREO

DISPLAY numEaten " Oreos equals " servings " servings!"
DISPLAY "You consumed " totalCalories " calories."

DISPLAY "Keep eating Oreos!"
