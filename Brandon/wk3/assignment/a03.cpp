/******************************************************************************
# Author:           Brandon Clyde
# Assignment:       Assignment 3
# Date:             01/30/2022
# Description:      This program prompts user for number of Oreos eaten and
#                   outputs the number of serving and calories those Oreos
#                   equal.
# Input:            int numEaten;
# Output:           double servings; int totalCalories;
# Sources:          https://www.geeksforgeeks.org/generics-in-c/
#*****************************************************************************/


#include <iostream>
#include <iomanip>
#include <limits>

/*** CONSTANT DECLARATIONS ***/
const int OREOS_PER_SERVING = 3;
const int CALORIES_PER_OREO = 53;


/*** FUNCTION DECLARATIONS ***/
template <typename T>
T validInput (std::string msg, std::string type);


/*** MAIN ***/
int main (void) {
  // Declare and initialize variables
  int numEaten = 0;
  double servings = 0.0;
  int totalCalories = 0;

  // Print program welcome statement
  std::cout << "Welcome to the Oreo Calculator!"
            << std::endl
            << std::endl;

  // Prompt user and store input in numEaten
  numEaten = validInput<int>("Enter the number of Oreos eaten: ", "int");

  // Find servings and totalCalories and save to respective variables
  servings = numEaten / static_cast<double>(OREOS_PER_SERVING);
  totalCalories = numEaten * CALORIES_PER_OREO;

  // Output message
  std::cout << std::endl
            << std::fixed << std::setprecision(1)
            << numEaten << " Oreos equals " << servings << " servings!" 
            << std::endl
            << "You consumed " << totalCalories << " calories."
            << std::endl
            << std::endl;

  // Ending message
  std::cout << "Keep eating Oreos!" 
            << std::endl;
    return 0;
}


/*** Function definitions ***/
/* 
validInput<type>:
  Prompts user with an input message then 
  verifies that the input is of type <type>.
  Loops until a valid input is entered.
  Returns a validated <type>.
  Function is a generic, return type is a parameter.
*/
template <typename T>
T validInput (std::string msg, std::string type) {
  T tmp;

  // Prompt user with "msg" and get user input
  std::cout << msg;
  std::cin >> tmp;

  // Check for an error on the input via cin, then loop until the input is valid
  while(true){
    if (std::cin.fail()){
      // Clear and ignore buffer so it is not an endless loop
      std::cin.clear();
      std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
      // Display error message and re prompt
      std::cout << "Invalid input! Input should be of type '" << type << "'" 
                << std::endl
                << std::endl
                << msg;
      std::cin >> tmp;
    }
    else{
      break;
    }    
  }

  // return validated input
  return tmp;
}
