/******************************************************************************
# Author:           Brandon Clyde
# Assignment:       Assignment 7
# Date:             03/06/2022
# Description:      This is a coffee shop menu program. It displays options for 
#                   users to make selections from and adds them to a running total.
#                   When the selection process is complete, it prompts for a tip and 
#                   outputs the total amount to be paid.
# Input:            (“pathNum”, int); (“selectChar”, char); (“tipAmount”, double);
# Output:           (“total”, double);
# Sources:          None
#*****************************************************************************/

#include <iostream>
#include <iomanip>
#include <cmath>

/*** CONSTANT DECLARATIONS ***/
const double DONUT_PRICE = 4.00;
const double MUFFIN_PRICE = 4.50;
const double PASTRY_PRICE = 5.50;
const double BAGEL_PRICE = 3.75;
const double TOAST_PRICE = 2.25;
const double COFFEE_PRICE = 3.50;
const double TEA_PRICE = 2.50;


/*** FUNCTION DECLARATIONS ***/
void menuPathPrompt();
double menuSweets();
double menuBread();
double menuDrinks();
double tipSuggestor(double total);


/*** MAIN ***/
int main(void){
  // Declare and initialize variables
  int pathNum = 0;
  double total = 0.0;

  // Set precision
  std::cout << std::fixed 
            << std::setprecision(2);

  // Welcome message
  std::cout << "Welcome to my Coffee Shop!"
            << std::endl;

  // Menu loop
  do{
    // Main menu choice
    menuPathPrompt();
    std::cin >> pathNum;

    // Validate pathNum
    while ((pathNum < 1 || pathNum > 4) || std::cin.fail()){
      std::cin.clear();
      std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
      std::cout << "Invalid Option! "
                << "Please choose 1-4!"
                << std::endl
                << ">> ";
      std::cin >> pathNum;
    }

    if (pathNum == 1){
      total += menuSweets();
      std::cout << "Total: "
                << total
                << std::endl;
    }
    else if (pathNum == 2){
      total += menuBread();
      std::cout << "Total: "
                << total
                << std::endl;
    }
    else if (pathNum == 3){
      total += menuDrinks();
      std::cout << "Total: "
                << total
                << std::endl;
    }
  } while (pathNum != 4);

  // Display pre-tip total
  std::cout << std::endl
            << "Your total: "
            << total
            << std::endl
            << std::endl;

  // Prompt and add tip
  total += tipSuggestor(total);

  // Display amount to be paid
  std::cout << std::endl
            << "Please pay $"
            << total
            << std::endl
            << std::endl;

  // Goodbye message
  std::cout << "Thank you for stopping by!"
            << std::endl;

  return 0;
}


/*** FUNCTION DEFINITIONS ***/
/*
menuPathPrompt:
  Displays possible menu paths and prompts
  the user to choose one of the paths.
  The function does not return value.
*/
void menuPathPrompt(){
  std::cout << std::endl
            << "Please pick an option below:"
            << std::endl
            << "    1. Donuts/Muffins/Pastries"
            << std::endl
            << "    2. Bagels/Toast"
            << std::endl
            << "    3. Coffee/Tea"
            << std::endl
            << "    4. Quit"
            << std::endl
            << ">> ";
}

/*
menuSweets:
  Displays possible menu options and prices for:
  donuts, muffins, and pastries.
  Prompts user to input a letter
  Returns the price of the user selection.
*/
double menuSweets(){
  // Declare variable
  char selectChar;

  // Initial prompt
  std::cout << "Would you like"
            << std::endl
            << "    D: Donuts $"
            << DONUT_PRICE
            << std::endl
            << "    M: Muffins $"
            << MUFFIN_PRICE
            << std::endl
            << "    P: Pastries $"
            << PASTRY_PRICE
            << std::endl
            << ">> ";

  // Get selection from user
  std::cin >> selectChar;

  // Validate selection          
  while ((tolower(selectChar) != 'd' && 
          tolower(selectChar) != 'm' &&
         tolower(selectChar) != 'p') || 
          std::cin.fail()
  ){
    std::cin.clear();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    std::cout << "Invalid Option!"
              << std::endl
              << "Would you like Donuts (D), Muffins (M), or Pastries (P): ";
    
    std::cin >> selectChar;
  }

  // Print item and return value
  if(selectChar == 'd'){
    std::cout << "Donuts added."
              << std::endl;
    return DONUT_PRICE;
  }
  else if(selectChar == 'm'){
    std::cout << "Muffins added."
              << std::endl;
    return MUFFIN_PRICE;
  }
  else if(selectChar == 'p'){
    std::cout << "Pastries added."
              << std::endl;
    return PASTRY_PRICE;
  }

  return 0.0;
}

/*
menuBread:
  Displays possible menu options and prices for:
  bagels and toast.
  Prompts user to input a letter
  Returns the price of the user selection.
*/
double menuBread(){
  // Declare variable
  char selectChar;

  // Initial prompt
  std::cout << "Would you like"
            << std::endl
            << "    B: Bagels $"
            << BAGEL_PRICE
            << std::endl
            << "    T: Toast $"
            << TOAST_PRICE
            << std::endl
            << ">> ";

  // Get selection from user
  std::cin >> selectChar;

  // Validate selection          
  while ((tolower(selectChar) != 'b' && 
         tolower(selectChar) != 't') || 
          std::cin.fail()
  ){
    std::cin.clear();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    std::cout << "Invalid Option!"
              << std::endl
              << "Would you like Bagels (B) or Toast (T): ";
    
    std::cin >> selectChar;
  }

  // Print item
  if(tolower(selectChar) == 'b'){
    std::cout << "Bagels added."
              << std::endl;
    return BAGEL_PRICE;
  }
  else if(tolower(selectChar) == 't'){
    std::cout << "Toast added."
              << std::endl;
    return TOAST_PRICE;
  }

  return 0.0;
}

/*
menuDrinks:
  Displays possible menu options and prices for:
  coffee and tea.
  Prompts user to input a letter
  Returns the price of the user selection.
*/
double menuDrinks(){
  // Declare variable
  char selectChar;

  // Initial prompt
  std::cout << "Would you like"
            << std::endl
            << "    C: Coffee $"
            << COFFEE_PRICE
            << std::endl
            << "    T: Tea $"
            << TEA_PRICE
            << std::endl
            << ">> ";

  // Get selection from user
  std::cin >> selectChar;

  // Validate selection          
  while ((tolower(selectChar) != 'c' && 
         tolower(selectChar) != 't') || 
          std::cin.fail()
  ){
    std::cin.clear();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    std::cout << "Invalid Option!"
              << std::endl
              << "Would you like Coffee (C) or Tea (T): ";
    
    std::cin >> selectChar;
  }

  // Print item
  if(tolower(selectChar) == 'c'){
    std::cout << "Coffee added."
              << std::endl;
    return COFFEE_PRICE;
  }
  else if(tolower(selectChar) == 't'){
    std::cout << "Tea added."
              << std::endl;
    return TEA_PRICE;
  }
  
  return 0.0;
}

/*
tipSuggestor:
  Displays suggested tip amounts.
  Prompts user to input a tip amount
  Returns the tip amount.
*/
double tipSuggestor(double total){
  // Declare variable
  double tipAmount = 0.0;

  // Tip suggestion
  std::cout << "Would you like to add a tip? "
            << "Suggested amounts:"
            << std::endl
            << std::endl
            << "10% = $"
            << total * 0.1
            << std::endl
            << "15% = $"
            << total * 0.15
            << std::endl
            << "20% = $"
            << total * 0.2
            << std::endl
            << std::endl;

  // Prompt for tip amount
  std::cout << "Enter tip amount: $";
  std::cin >> tipAmount;

  while(std::cin.fail() || tipAmount < 0){
    std::cin.clear();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    std::cout << "Invalid input! Please enter a number 0 or greater: ";

    std::cin >> tipAmount;
  }

  return tipAmount;
}