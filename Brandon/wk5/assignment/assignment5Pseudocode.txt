DECLARE string textMsg

DECLARE map<string, string> translations
SET translations = {
	{“BFF”, “best friend forever”},
	{“IDK”, ”I don't know”},
	{“JK”, ”just kidding”},
	{“TMI”, ”too much information”},
	{“TTYL”, ”talk to you later”},
	{“RN”, ”right now”},
	{“FR”, ”for real”}
}


DISPLAY “Welcome to the Text Message Decoder!”

DISPLAY “Enter a single line text message:”
INPUT textMsg

DISPLAY "You entered: " textMsg

FOR abbr in translations
	IF (textMsg.find(abbr.first) != std::string::npos) THEN
		DISPLAY abbr.first ": " abbr.second
	END IF
END FOR

DISPLAY "END."
