/******************************************************************************
# Author:           Brandon Clyde
# Assignment:       Assignment 5
# Date:             02/09/2022
# Description:      This program translates abbreviations in text messages to
#                   to their full versions. It takes in a single line text message,
#                   finds any supported abbreviations, and outputs the original  
#                   messages with any abbreviations translated below.
# Input:            (“textMsg”, string);
# Output:           (“textMsg”, string); (“abbreviation”, string); (“translation”, string);
# Sources:          https://stackoverflow.com/questions/27775233/lambdas-and-capture-by-reference-local-variables-accessing-after-the-scope
#*****************************************************************************/

#include <iostream>
#include <string>
#include <map>
#include <algorithm>
#include <functional>


/*** MAIN ***/
int main (void){
  // Declare input
  std::string textMsg = "";

  // Build translations map
  std::map<std::string, std::string> translations = {
    {"BFF", "best friend forever"},
    {"IDK", "I don't know"},
    {"JK", "just kidding"},
    {"TMI", "too much information"},
    {"TTYL", "talk to you later"},
    {"RN", "right now"},
    {"FR", "for real"}
  };

  std::cout << "Welcome to the Text Message Decoder!"
            << std::endl
            << std::endl;

  // Prompt user for input and get line of text from user
  std::cout << "Enter a single line text message: ";
  getline(std::cin, textMsg);

  // Print the user input
  std::cout << std::endl
            << "You entered: "
            << textMsg
            << std::endl;

  // Iterate through the translations map and check if the abbr key (abbreviation) is in the message, 
  // output the abbr key (abbreviation) and value (translation) if so.
  std::for_each(translations.begin(), translations.end(), [&](std::pair<std::string, std::string> abbr) {
    if (textMsg.find(abbr.first) != std::string::npos){
      std::cout << abbr.first
                << ": "
                << abbr.second
                << std::endl;
    }
  });

  // End message
  std::cout << std::endl
            << "END."
            << std::endl;

  return 0;
}